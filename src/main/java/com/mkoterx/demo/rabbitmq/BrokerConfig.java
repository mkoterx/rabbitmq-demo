package com.mkoterx.demo.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class BrokerConfig {

    public final static String QUEUE_NAME = "test";
    public final static String BROKER_HOST = "localhost";
    public final static int BROKER_PORT = 5672;
    private static ConnectionFactory connectionFactory;

    public static Channel getFreshChannel() throws IOException, TimeoutException {
        connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(BROKER_HOST);
        connectionFactory.setPort(BROKER_PORT);
        Connection conn = connectionFactory.newConnection();
        return conn.createChannel();
    }
}
