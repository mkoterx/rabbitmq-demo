package com.mkoterx.demo.rabbitmq;

import com.rabbitmq.client.*;

import static com.mkoterx.demo.rabbitmq.BrokerConfig.*;

public class Receiver {



    public static void main(String[] argv) throws Exception {
//        consumeAutoAckTrue();
        consumeAutoAckFalse();
    }

    public static void consumeAutoAckFalse() throws Exception {
//        Connection connection = getFreshChannel();
        Channel channel = getFreshChannel();

        boolean durable = true;
        channel.queueDeclare(QUEUE_NAME, durable, false, false, null);
        System.out.println(String.format(" [*] Waiting for messages on queue [%s]. To exit press CTRL+C", QUEUE_NAME));

        int prefetchCount = 1;
        channel.basicQos(prefetchCount);

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println(" [x] Received '" + message + "'");
            try {
                Thread.sleep(3*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            System.out.println(" [x] Acknowledged message '" + message + "'");
        };
        channel.basicConsume(QUEUE_NAME, false, deliverCallback, consumerTag -> {});
    }

    public static void consumeAutoAckTrue() throws Exception {
//        Connection connection = getConnection();
        Channel channel = getFreshChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println(" [x] Received '" + message + "'");
        };
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> {
        });
    }

}
