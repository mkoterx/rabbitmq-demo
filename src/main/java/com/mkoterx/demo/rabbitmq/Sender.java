package com.mkoterx.demo.rabbitmq;


import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.MessageProperties;

import static com.mkoterx.demo.rabbitmq.BrokerConfig.*;

public class Sender {

    public static void main(String[] argv) throws Exception {
        try (
             Channel channel = getFreshChannel()) {
            boolean durable = true;
            channel.queueDeclare(QUEUE_NAME, durable, false, false, null);
            for (int i = 1; i <= 10; i++) {
                String message = "Message " + i;
                channel.basicPublish("", QUEUE_NAME,  MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());
            }
        }
    }
}
